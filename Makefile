all:
	(cd serverapp; make all)
	(cd clientapp; make all)

clean:
	(cd serverapp; make clean)
	(cd clientapp; make clean)
