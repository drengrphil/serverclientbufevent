/*
 * @Author:  Phillip
 */

#ifndef DATATLVENCODER_H
#define DATATLVENCODER_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// data stream types.
#define STRING_STREAM_TYPE 0X01
#define CHAR_STREAM_TYPE 0x02

#define comparekey(x, y) ((x==y)?1:0)

/*
 * The following definitions are used for in-memory representation of a TLV
 */
typedef struct DataVal
{
	void *dValue;
} DataValue;

typedef int keyVal;
typedef void (*dataValueGen)(DataValue dValue);

typedef struct KeyValueList
{
	keyVal key;
	DataValue dvalue;
	struct KeyValueList *prev;
	struct KeyValueList *next;
} KeyValueNode;


typedef struct KeysNode
{
	int numkeys;
	KeyValueNode *keyHeader;
	dataValueGen valGen;
} KeysNodeList;

typedef struct ClientPayload
{
	int dataType;
	int dataLength;
	unsigned char *dataValue;
} ClientPayload;

typedef struct PacketizedData
{
	KeysNodeList *dataList;
	unsigned char *dataSerializedBuffer;
	int nSerializedBytes;
} PacketizedData;

#define foreach_keynodelist(Node, Value) KeyValueNode *node = NULL;\
      KeyValueNode *Value;\
      for (Value = node = Node->keyHeader; node != NULL; Value = node = node->next)


/*
 * Helper functions to operate on the memory representation
 * of the TLV structs.
 */
static KeysNodeList *GenerateKeys(dataValueGen valGen)
{
	KeysNodeList *kList = (KeysNodeList*)malloc(sizeof(KeysNodeList));
	kList->numkeys = 0;
	kList->keyHeader = NULL;
	kList->valGen = valGen;
	return kList;
}

static  KeyValueNode *KeyListFindKey(KeysNodeList *kList, keyVal key)
{
	KeyValueNode *currNode = kList->keyHeader;
	while ( currNode != NULL)
	{
		if (comparekey(key, currNode->key))
		{
			return currNode;
		}
		currNode = currNode->next;
	}
	return NULL;
}


static int AddKey(KeysNodeList *kList, keyVal key, DataValue dVal)
{
	if (KeyListFindKey(kList, key))
	{
		return -1;
	}

	KeyValueNode *newNode = calloc(1, sizeof(KeyValueNode));
	if (newNode == NULL)
	{
		return -1;
	}

	newNode->key = key;
	newNode->dvalue = dVal;
	newNode->prev = NULL;
	newNode->next = NULL;

	if (kList->keyHeader != NULL)
	{
		newNode->next = kList->keyHeader;
		kList->keyHeader->prev = newNode;
	}
	kList->keyHeader = newNode;
	kList->numkeys++;

	return 0;
}

static void RemovePacketizedDataValue(DataValue dval)
{
	ClientPayload *cPayload = (ClientPayload*)dval.dValue;
	free(cPayload->dataValue);
	free(cPayload);
}

static PacketizedData *EncodeTlvData()
{
	PacketizedData *packet = (PacketizedData*)malloc(sizeof(PacketizedData));
	packet->dataList = GenerateKeys(RemovePacketizedDataValue);
	packet->dataSerializedBuffer = NULL;
	packet->nSerializedBytes = 0;
	return packet;
}

static int PacketObject(PacketizedData *packet, int type, void *value, int len)
{
	if (packet->dataSerializedBuffer != NULL)
	{
		return -1;
	}

	ClientPayload *cPayload = (ClientPayload*)malloc(sizeof(ClientPayload));
	cPayload->dataType = type;
	cPayload->dataLength = len;
	cPayload->dataValue = (unsigned char*)malloc(len);
	memcpy(cPayload->dataValue, value, len);
	DataValue dV;
	dV.dValue = cPayload;

	if (AddKey(packet->dataList, type, dV) < 0)
	{
		return -1;
	}

	packet->nSerializedBytes += sizeof(int) * 2 + len;
	return 0;
}


/*
 * Resources freeing and clean ups.
 */
static int DeleteKeyList(KeysNodeList *kList)
{
	KeyValueNode *currNode = kList->keyHeader;
	while (currNode != NULL)
	{
		KeyValueNode *next = currNode->next;
		kList->valGen(currNode->dvalue);
		free(currNode);
		currNode = next;
	}
	free(kList);
	return 0;
}

static int *DeleteEcondedTlvData(PacketizedData *pcktData)
{
	DeleteKeyList(pcktData->dataList);
	if (pcktData->dataSerializedBuffer != NULL)
	{
		free(pcktData->dataSerializedBuffer);
	}

	free(pcktData);
	return 0;
}

#endif  // DATATLVENCODER_H
