Build
---------
To build the client and server programs in one command, use the 'make' command
in the top-level directory.

Server
---------

The server source code can be found in serverapp/server.c

To start the server:
./YeahServer portNum


Client
----------

The client source code can be found in clientapp/client.c

To start the client:
./YeahClient portNum
