#ifndef CLIENT_H
#define CLIENT_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>       // hostent
#include <string.h>
#include <stdlib.h>
#include <event2/event.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>

// Report errors.
#define errreport(errmsg) {perror(errmsg); exit(1);}

// Maximum buffer size allowed.
#define BUFSIZE 1024

// Maximum length of hostname.
#define HOSTNAMELEN 256


/*
 * Client Connection and session handler.
 */
extern void ClientEventOp(struct bufferevent *bEvent, short ev, void *clientDetials);
extern void ClientSendOp(struct bufferevent *bEvent, void *clientDetials);
extern void ClientReceiveOp(struct bufferevent *bEvent, void *clientDetials);
extern void ClientConnectionSetup(unsigned short int portNum);


#endif

