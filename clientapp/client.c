/*
 * @Author:  Phillip B. Oni
 */

#include "client.h"
#include "../dataencoder/datatlvencoder.h"

/*
 * Helper functions to encode and serialize data for sending.
 */
static int EncodeStringData(PacketizedData *pcktData, int type, char *dValue)
{
        return PacketObject(pcktData, type, dValue, strlen(dValue)+1);
}

// Serialize encoded TLV stream.
static int SerializeEncodedData(PacketizedData *pcktData)
{
        if (pcktData->dataSerializedBuffer != NULL)
        {
                return -1;
        }
        int dOffset = 0;
        unsigned char* outBuff = (unsigned char*)malloc(pcktData->nSerializedBytes);
        foreach_keynodelist(pcktData->dataList, dnode)
        {
                ClientPayload *cPayload = (ClientPayload*)dnode->dvalue.dValue;
                memcpy(outBuff+dOffset, &cPayload->dataType, sizeof(int));
                dOffset += sizeof(int);
                memcpy(outBuff+dOffset, &cPayload->dataLength, sizeof(int));
                dOffset += sizeof(int);
                memcpy(outBuff+dOffset, cPayload->dataValue, cPayload->dataLength);
                dOffset += cPayload->dataLength;
        }
        pcktData->dataSerializedBuffer = outBuff;
        return 0;
}

/*
 * Function to prepare a TLV packet for transmission.
 */
PacketizedData *PrepareDataForTransmission()
{
    // TLV Data packaging
    PacketizedData *packet = EncodeTlvData();
    // TODO: replace hardcoded message with variable message lengths.
    const char *clientPayload_1 = "Hello";
    const char *clientPayload_2 = "Great Job!";
    const char *clientPayload_3 = "Goodbye";
    // Package the messages into a single packet.
    EncodeStringData(packet, STRING_STREAM_TYPE, (char*) clientPayload_1);
    EncodeStringData(packet, STRING_STREAM_TYPE, (char*) clientPayload_2);
    EncodeStringData(packet, STRING_STREAM_TYPE, (char*) clientPayload_3);
    // Serialize the packet
    if (SerializeEncodedData(packet) < 0)
    {
	printf("Failed to serialize packet!");
	return NULL;
    }

    // Check bytes written to outgoing buffer.
    printf("Packet of size %d bytes successfully serialized and sent\n", packet->nSerializedBytes);
    return packet;
}

/*
 * Section: Connection setup and data transmission from client side.
 */
void ClientEventOp(struct bufferevent *bEvent, short ev, void *clientDetials)
{
        if (ev & BEV_EVENT_EOF)
        {
                printf("Connection to server is closed.\n");
        }
        else if (ev & BEV_EVENT_CONNECTED)
        {
                // Everything is looking great with connection.
                printf("Connection Established!\n");
                // Data to send to server.
		PacketizedData *packet = PrepareDataForTransmission();
                // Send to server.
		bufferevent_write(bEvent, packet, packet->nSerializedBytes);
		DeleteEcondedTlvData(packet);
		return;
        }
        else if (ev & BEV_EVENT_ERROR)
        {
                printf("Something went wrong with connection\n");
        }
        else
        {
                printf("Unknown Error!\n");
        }

        bufferevent_free(bEvent);
}

void ClientSendOp(struct bufferevent *bEvent, void *clientDetials)
{
}

// Feedback from Server, if any.
void ClientReceiveOp(struct bufferevent *bEvent, void *clientDetials)
{
        char rxBuffer[BUFSIZE]; // Incoming or input buffer.
        char txBuffer[BUFSIZE]; // Outgoing buffer.
        memset(rxBuffer, 0x00, sizeof(rxBuffer));
        struct evbuffer *inBuff = bufferevent_get_input(bEvent);
        size_t bufSize = evbuffer_get_length(inBuff);
        if (bufSize > 0)
        {
                bufferevent_read(bEvent, rxBuffer, bufSize);
                printf("From Server: %s", rxBuffer);
                // Respond back to server.
                memset(txBuffer, 0, sizeof(txBuffer));
                snprintf(txBuffer, sizeof(txBuffer)-1, "Thank you server");
                bufferevent_write(bEvent, txBuffer, strlen(txBuffer));

        }
}

/*
 * Function to connect client to server.
 */
void ClientConnectionSetup(unsigned short int portNum)
{
        struct event_base *evBase;
        struct sockaddr_in sockAddr;
        evBase = event_base_new();
        if (!evBase)
        {
                errreport("event");
                return;
        }

        memset(&sockAddr, 0, sizeof(sockAddr));
        // Since we are connecting to localhost.
        sockAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
        sockAddr.sin_family = AF_INET;
        sockAddr.sin_port = portNum;

        struct bufferevent* bEvent = bufferevent_socket_new(evBase, -1, BEV_OPT_CLOSE_ON_FREE);
        if (!bEvent){ printf("Client Socket initialization failed\n"); errreport("event"); }

        bufferevent_setcb(bEvent, ClientReceiveOp, ClientSendOp, ClientEventOp, NULL);

        // Connect to the server and send data.
        int connID = bufferevent_socket_connect(bEvent, (struct sockaddr*) &sockAddr, sizeof(sockAddr));
        if (connID < 0){ printf("Client Connection to server failed.\n"); errreport("connect");}
        bufferevent_enable(bEvent, EV_READ | EV_WRITE);
        // Event dispatcher.
        event_base_dispatch(evBase);
        event_base_free(evBase);
}


int main(int argn, char *args[])
{
	// Start client connection to the server.
	ClientConnectionSetup(htons(atoi(args[1])));
	return 0;
}
