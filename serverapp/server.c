/*
 * @Author:  Phillip B. Oni
 */

#include "server.h"
#include "../dataencoder/datatlvencoder.h"

static int GetKey(KeysNodeList *kList, keyVal key, DataValue *dVal)
{
        KeyValueNode *node = KeyListFindKey(kList, key);
        if (node == NULL)
        {
                return -1;
        }
        *dVal = node->dvalue;
        return 0;
}

/*
 * Function parse the received TLV packet at the server.
 */
static PacketizedData *ParseEcondedTlvData(unsigned char *inBuffer, int bufSize)
{
        PacketizedData *packet = EncodeTlvData();
        unsigned char *packetCache = (unsigned char*)malloc(bufSize);
        memcpy(packetCache, inBuffer, bufSize);
        int dOffset = 0;
        while (dOffset < bufSize)
        {
                int dtype = (*(int*)(packetCache + dOffset));
                dOffset += sizeof(int);
                int dlength = (*(int*)(packetCache + dOffset));
                dOffset += sizeof(int);
                PacketObject(packet, dtype, packetCache+dOffset, dlength);
                dOffset += dlength;
        }
        packet->dataSerializedBuffer = packetCache;
        packet->nSerializedBytes = bufSize;
        return packet;
}

/*
 * Decode string stream.
 */
static int DecodeInputStringStream(
                PacketizedData *pcktData,
                int type,
                char *dataVal,
                int *stringLen)
{
        DataValue dval;
        if (GetKey(pcktData->dataList, type, &dval) != 0)
        {
                return -1;
        }
        ClientPayload *cPayload = (ClientPayload*) dval.dValue;
        if (*stringLen < cPayload->dataLength)
        {
                return -1;
        }
        memset(dataVal, 0, *stringLen);
        *stringLen = cPayload->dataLength;
        memcpy(dataVal, cPayload->dataValue, cPayload->dataLength);
	printf("Length of received data: %d\n", cPayload->dataLength);
        return 0;
}

/*
 *  Unserialize received packet for decoding.
 */
static void UnserializeReceivedPacket(PacketizedData *inPacket)
{
	PacketizedData *unserializedPacket = ParseEcondedTlvData(inPacket->dataSerializedBuffer, inPacket->nSerializedBytes);
	if (unserializedPacket == NULL)
	{
		printf("Parsed Data is null\n");
		return;
	}
	// Get Data out
	char receivedMessage[BUFSIZE];
	memset(receivedMessage, 0, sizeof(receivedMessage));
	int bufLen = 128;
	if (DecodeInputStringStream(unserializedPacket, STRING_STREAM_TYPE, receivedMessage, &bufLen) < 0)
	{
		printf("Error decoding packet.\n");
		return;
	}
	printf("Size: %ld", strlen(receivedMessage));
	printf("Received: %s \n", receivedMessage);
	// Any possible mem leak?
	DeleteEcondedTlvData(unserializedPacket);
}

/*
 * Function to process incoming buffer and print its content.
 * It is assumed that server does not send any ACK frame back to
 *   the client.
 *   TODO: Out-buffer could be added to allow server respond back
 *   to the client with an ACK frame. And buffer overflow could also
 *   be monitored and prevented.
 */
void BuffIncomingMessage(struct bufferevent *buffEv, void *conn)
{
	struct evbuffer *inBuff;
	// Received Message from client
	PacketizedData *receivedPacket = EncodeTlvData();
	inBuff = bufferevent_get_input(buffEv); // To read.
	// Lenght of received message
	size_t rxMsgSize  = evbuffer_get_length(inBuff);
	if (rxMsgSize > 0)
	{
		bufferevent_read(buffEv, receivedPacket, rxMsgSize);
		UnserializeReceivedPacket(receivedPacket);
	}
}

/*
 * Function to accept incoming connections from clients; uses nonblocking approach.
 * Accepts multiple concurrent connections from clients.
 */
void AcceptConnection(evutil_socket_t sockListener, short ev, void *arg)
{
	struct event_base *evBase = arg;
	struct sockaddr_storage sockAddrSto;
	socklen_t sockLen = sizeof(sockAddrSto);
	
	int connfd = accept(sockListener, (struct sockaddr*) &sockAddrSto, &sockLen);
	if (connfd < 0)
	{
		errreport("accept error");
	}
	else if (connfd > FD_SETSIZE)
	{
		close(connfd);
	}
	else
	{
		printf("New Client Connection Accepted\n");
		struct bufferevent *bufEv;
		evutil_make_socket_nonblocking(connfd);
		bufEv = bufferevent_socket_new(evBase, connfd, BEV_OPT_CLOSE_ON_FREE);
		bufferevent_setcb(bufEv, BuffIncomingMessage, NULL, EventErrorHandler, NULL);
		bufferevent_setwatermark(bufEv, EV_READ, 0, BUFSIZE);
		bufferevent_enable(bufEv, EV_READ|EV_WRITE);
	}
}

/*
 * Handle potential event/connect error.
 */
void EventErrorHandler(struct bufferevent *buffEv, short error, void *conn)
{
	if (error & BEV_EVENT_EOF)
	{
		bufferevent_free(buffEv);
		printf("Connection to Server is closed.");
	}
	else if (error & BEV_EVENT_TIMEOUT)
	{
		printf("Server Time out");
	}
	else if (error & BEV_EVENT_ERROR)
	{
		printf("Socket event error");
		// EVUTIL_SOCKET_ERROR()
	}
	bufferevent_free(buffEv);
}

/*
 * Server host creates socket and listens on the socket
 */
void StartServerConnection(unsigned short int portNum)
{
	/*
         * Construct address.
         */
        struct sockaddr_in sockAddr;
        evutil_socket_t sockListener;
        struct event_base *evBase;
        struct event *evListener;

        /*
         * Listener on the socket (TCP)
         */
        evBase = event_base_new();
        if (!evBase)
        {
		errreport("event");
                return;
	}

        sockAddr.sin_family = AF_INET; // Socket type
        sockAddr.sin_addr.s_addr = INADDR_ANY; // Or set = 0
        sockAddr.sin_port = portNum; // Port number specified.
        sockListener = socket(AF_INET, SOCK_STREAM, 0); // TCP Streams
        // Nonblocking - taking multiple concurrent connections.
        evutil_make_socket_nonblocking(sockListener);

        /*
         * Bind the TCP socket to address and portnum.
         */
        int bindRes = bind(sockListener, (struct sockaddr*) &sockAddr, sizeof(sockAddr));
        if (bindRes == -1)
        {
                errreport("bind");
                return;
        }

	int listenRes = listen(sockListener, 16);
        if (listenRes == -1)
        {
                errreport("listen");
                return;
        }

        // New Connection Handler.
        evListener = event_new(evBase, sockListener, EV_READ|EV_PERSIST, AcceptConnection, (void*)evBase);
	event_add(evListener, NULL);
        event_base_dispatch(evBase);
}

int main(int argn, char *args[])
{
        // Start server.
        StartServerConnection(htons(atoi(args[1])));
        return 0;
}
