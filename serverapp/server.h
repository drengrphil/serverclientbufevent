// 
#ifndef CONNECTINFO_H
#define CONNECTINFO_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>       // hostent
#include <string.h>
#include <stdlib.h>
#include <event2/event.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>

// Report errors.
#define errreport(errmsg) {perror(errmsg); exit(1);}
// Maximum buffer size allowed.
#define BUFSIZE 1024
// Maximum length of hostname.
#define HOSTNAMELEN 256

// Accept incoming connections.
extern void AcceptConnection(evutil_socket_t sockListener, short ev, void *arg);
// Get message from client and buffered it.
extern void BuffIncomingMessage(struct bufferevent *buffEv, void *conn);
// Event error handler.
extern void EventErrorHandler(struct bufferevent *buffEv, short error, void *conn);
extern void StartServerConnection(unsigned short int portNum);
#endif
